import java.util.concurrent.atomic.AtomicInteger;
import java.io.InputStreamReader;

public class WaitProcessThread extends Thread {
  private AtomicInteger lock;
  private Process process;
  
  WaitProcessThread(AtomicInteger lock, Process process) {
    this.lock = lock;
    this.process = process;
  }

  @Override
  public void run() {
    synchronized(this){
       try {
        int result = process.waitFor();
        if (result > 0) {
          println("Erro ("+result+") ao executar o script python da WaitProcessThread.");
          BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
          BufferedReader stdError = new BufferedReader(new InputStreamReader(process.getErrorStream()));
          String s = null;
          try{
            while ((s = stdInput.readLine()) != null) {
                System.out.println(s);
            }
            while ((s = stdError.readLine()) != null) {
                println(s);
            }
          }catch(Exception e) {println("Problema ao exibir a mensagem de erro do processo.");}
        }
      } catch (InterruptedException e) { println("Erro na classe WaitProcessThread.");}
      this.lock.decrementAndGet();
    }
  }
}
