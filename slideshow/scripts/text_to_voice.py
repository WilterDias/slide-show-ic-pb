import sys
from io import BytesIO
import gtts
import pyglet

global player

def update(dt):
  if not player.playing:
    pyglet.app.exit()



msg = ""
if (len(sys.argv) > 0):
	msg = sys.argv[1]

mp3Bytes = BytesIO()
tts = gtts.gTTS(text=msg, lang="pt", slow=False)
tts.write_to_fp(mp3Bytes)
mp3Bytes.seek(0)

player = pyglet.media.Player() 
sound = pyglet.media.load(None, file=mp3Bytes, streaming=False)
player.queue(sound) 
player.play()

pyglet.clock.schedule_interval(update, 1/10.0)
pyglet.app.run()