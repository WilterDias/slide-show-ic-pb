# pip install pyppeteer
# Na primeira vez esse script instala o chromium, onde somente ele usa.

import sys
import asyncio
from pyppeteer import launch


async def page_web_screenshot(url: str, path: str, viewport_width: int, viewport_height: int, device_scale_factor: int):
    browser = await launch({
        'headless': True,
        'args': ['--lang=pt-BR']
    })
    page = await browser.newPage()
    await page.setViewport({'width': viewport_width, 'height': viewport_height, 'deviceScaleFactor': device_scale_factor})
    await page.goto(url, {'timeout': 10*60*1000, 'waitUntil': 'networkidle0'})
	#await page.waitForNavigation({'waitUntil': 'networkidle2'})
    await asyncio.sleep(10)
    await page.screenshot({'path': path})
    await browser.close()

arg = ""
asyncio.get_event_loop().run_until_complete(page_web_screenshot(sys.argv[1], sys.argv[2], int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5])))