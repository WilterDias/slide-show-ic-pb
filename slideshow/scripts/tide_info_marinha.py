# python -m pip install --upgrade pip
# python -m pip install beautifulsoup4
# python -m pip install datetime
# pip install requests
# pip install pdfminer.six
# pip install pandas
from io import StringIO
from datetime import date

import os
import sys
import re
import requests
from bs4 import BeautifulSoup, Tag
#import pandas

from pdfminer.converter import TextConverter
from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.pdfpage import PDFPage

def obtem_link_tabua_mare_cabedelo(doc_html: any, year: int):
    pattern: str = re.compile('PORTO DE CABEDELO - '+str(year))
    return doc_html.findAll('td', string={pattern})\
        .pop(0)\
        .parent\
        .select_one('td[class="views-field views-field-field-ficha-mare views-align-center"]')\
        .a['href']


def obtem_html(url: str):
    html = requests.get(url, headers = {"Accept-Language": "en-US,en;q=0.5"})
    if (html.status_code != 200):
        print('Erro na obtenção do HTML do site.')
    return BeautifulSoup(html.content, 'html.parser')

def download_archive(url, address=None):
    if address is None:
        address = os.path.basename(url.split("?")[0])
    response = requests.get(url)
    if response.status_code == requests.codes.OK:
        with open(address, 'wb') as archive:
            archive.write(response.content)
        #print("Arquivo armazenado em: {0}".format(address))
    else:
        response.raise_for_status()
        print("Erro ao baixar arquivo.")


def convert_pdf_to_txt(path):
    rsrcmgr = PDFResourceManager()
    retstr = StringIO()

    device = TextConverter(rsrcmgr, retstr, codec='utf-8', laparams=None)#LAParams())
    fp = open(path, 'rb')
    interpreter = PDFPageInterpreter(rsrcmgr, device)

    for page in PDFPage.get_pages(fp, set(), maxpages=0, password="",caching=True, check_extractable=True):
        interpreter.process_page(page)

    text = retstr.getvalue()

    fp.close()
    device.close()
    retstr.close()
    return text

arg = ""
if (len(sys.argv) > 0):
    arg = sys.argv[1]

path = arg + 'arquivos/cabedelo'
download_archive(obtem_link_tabua_mare_cabedelo(obtem_html("https://www.marinha.mil.br/chm/tabuas-de-mare"), date.today().year),
                 path+'.pdf')
text = convert_pdf_to_txt(path+'.pdf')

text = re.sub(' +', ' ', text) #substitui multiplos espaços em branco em um único espaço em branco
replacements = [(" HORA ALT (m)", ""), #diz as palavras que devem ser substituídas por vazio
                ("Janeiro", ""),
                ("Fevereiro", ""),
                ("Março", ""),
                ("Abril", ""),
                ("Maio", ""),
                ("Junho", ""),
                ("Julho", ""),
                ("Agosto", ""),
                ("Setembro", ""),
                ("Outubro", ""),
                ("Novembro", ""),
                ("Dezembro", "")]
var = [text := text.replace(a, b) for a, b in replacements] #faz a substituição pela lista anterior
delete_end = [m.start() for m in re.finditer("[^\s]+Original", text)] #Obtém a referência para a remoção do texto indesejável #outra forma de usar o regex (\d\.\d)([^\s]+?)Original
delete_ini_temp = [m.start() for m in re.finditer('(01\w{3}\d{4})', text)] #'(01SEG\d{4} +|01TER\d{4} +|01QUA\d{4} +|01QUI\d{4} +|01SEX\d{4} +|01SAB\d{4} +|01DOM\d{4})', text)]
offset_cut = delete_ini_temp[0]
delete_ini = []
i = 0
for it in delete_ini_temp:
    if i < len(delete_ini_temp) and delete_end[i] < it:
        delete_ini.append(it)
        i += 1
text = text[0:(delete_end[len(delete_end)-1]+3)]
delete_end = delete_end[:-1]
for i in range(len(delete_ini)):
    text = text[0:(delete_end[1-i]+3)] + text[(delete_ini[len(delete_ini)-1-i]):]
year = text[offset_cut-4:offset_cut]
text = text[offset_cut:]

text = re.sub(r'(01)(\w{3})(\d{4})', r'#\1 \2 \3',text) #separa os meses
text = re.sub(r'(\d{2})(\w{3})(\d{4})', r'@\1 \2 \3',text)[1:] #separa os dias

csv = "ano,mes,dia,dia da semana,mare 1,nivel 1,mare 2,nivel 2,mare 3,nivel 3,mare 4,nivel 4,lua\n"
months = text.split('#')
for month in range(len(months)):
    days = months[month].split('@')
    for d in range(len(days)):
        day_format = None
        if (len(days[d]) == 31):
            day_format = re.sub(r'^(\d{2})\s(\w{3})\s(\d{2})(\d{2})\s(\d\.\d)'
                                r'(\d{2})(\d{2})\s(\d\.\d)'
                                r'(\d{2})(\d{2})\s(\d\.\d)', r'{0},{1},\1,\2,\3:\4,\5,\6:\7,\8,\9:\10,\11,,,'.format(year,month+1), days[d])
        if (len(days[d]) == 39):
            day_format = re.sub(r'^(\d{2})\s(\w{3})\s(\d{2})(\d{2})\s(\d\.\d)'
                                r'(\d{2})(\d{2})\s(\d\.\d)'
                                r'(\d{2})(\d{2})\s(\d\.\d)'
                                r'(\d{2})(\d{2})\s(\d\.\d)', r'{0},{1},\1,\2,\3:\4,\5,\6:\7,\8,\9:\10,\11,\12:\13,\14,'.format(year,month+1), days[d])
        if (len(days[d]) == 40):
            day_format = re.sub(r'^(\d{2})\s(\w{3})\s(\d{2})(\d{2})\s(\d\.\d)'
                                r'(\d{2})(\d{2})\s(\d\.\d)'
                                r'(\d{2})(\d{2})\s(\d\.\d)'
                                r'(\d{2})(\d{2})\s(\d\.\d)(.)', r'{0},{1},\1,\2,\3:\4,\5,\6:\7,\8,\9:\10,\11,\12:\13,\14,~\15'.format(year,month+1), days[d])
            day_format = day_format.replace("~v","minguante")
            day_format = day_format.replace("~&","nova")
            day_format = day_format.replace("~c","crescente")
            day_format = day_format.replace("~z","cheia")

        csv += day_format+"\n"

arquivo = open(path + ".csv", "wt", encoding="utf8")
arquivo.writelines(csv)
arquivo.close()

#arquivo= open('cabedelo.csv', 'r')
#df = pandas.read_csv (StringIO(arquivo.read()), sep=",")
#arquivo.close()
#df = pandas.read_csv (StringIO(csv))
#pandas.set_option('display.max_rows', None)
#pandas.set_option('display.max_columns', None)
#pandas.set_option('display.width', None)
#pandas.set_option('display.max_colwidth', None)

#print(df)