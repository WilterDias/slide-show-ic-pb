import java.util.concurrent.atomic.AtomicInteger;

public class WebPageScreen {
  private String url;
  private String name;
  private int viewportWidth;
  private int viewportHeight;
  private int deviceScaleFactor;
  private long waitTime;
  private long timeStamp;
  
  private boolean waitProcess;
  private AtomicInteger lock;
  private Thread waitProcessThread;
  
  public WebPageScreen(String url,
                       String name,
                       int viewportWidth,
                       int viewportHeight,
                       int deviceScaleFactor,
                       int waitTimeWebPage) {
    this.url = url;
    this.name = name;
    this.viewportWidth = viewportWidth;
    this.viewportHeight = viewportHeight;
    this.waitTime = waitTimeWebPage;
    this.waitProcess = true;
    this.deviceScaleFactor = deviceScaleFactor;
    this.lock = new AtomicInteger(1);
    executePython();
    if (waitProcess) {
      try {
        waitProcessThread.join();//enquanto a thread não morrer
      } catch (Exception e){println("Erro no construtor da classe WebPageScreen.");}
    }
    this.timeStamp = millis();
  }
  
  private void executePython() {
    boolean isWindowsSo = System.getProperty("os.name").toLowerCase().contains("windows");
    Process p = exec(isWindowsSo? "python": "python3", sketchPath("") + "scripts/screenshot_webpage.py", url, sketchPath("") + name, String.valueOf(this.viewportWidth), String.valueOf(this.viewportHeight), String.valueOf(deviceScaleFactor)); //Se não tiver instalado o python e os pacotes dará problema.
    //println("python "+sketchPath("") + "scripts/screenshot_webpage.py "+url+" "+sketchPath("") + name+" "+String.valueOf(this.viewportWidth)+" "+String.valueOf(this.viewportHeight)+" "+String.valueOf(deviceScaleFactor));
    waitProcessThread = new WaitProcessThread(lock, p); //o lock e o processo é passado por referência
    waitProcessThread.start();
  }
  
  public void refreshImage () {
    if (millis() - this.timeStamp >= waitTime) {
      if (lock.get() == 0) {
        lock.incrementAndGet();
        executePython();
        if (waitProcess) {
          try {
            waitProcessThread.join();//enquanto a thread não morrer
          } catch (Exception e){println("Erro no método refreshImage da classe WebPageScreen.");}
        }
        this.timeStamp = millis();
      }
    }
  }
  
  public String getUrl() {
    return this.url;
  }
  
  public String getName() {
    return this.name;
  }
  
  public int getViewportWidth() {
    return this.viewportWidth;
  }
  
  public int getViewportHeight() {
    return this.viewportHeight;
  }
  
  public long getWaitTime() {
    return this.waitTime;
  }
  
  public boolean getWaitProcess() {
    return this.waitProcess;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public void setViewportWidth(int viewportWidth) {
    this.viewportWidth = viewportWidth;
  }
  
  public void setViewportHeight(int viewportHeight) {
    this.viewportHeight = viewportHeight;
  }
  
  public void setWaitTime(long waitTime) {
    this.waitTime = waitTime;
  }
  public void setWaitProcess(boolean waitProcess) {
    this.waitProcess = waitProcess;
  }
}
