import java.io.File;
import VLCJVideo.*;


HashMap<Integer, Frame> quadros;
PGraphics pg;

long tempoAgora;
int contador;
int contadorAnterior;
long tempoUltimoDraw;

public void settings() {
  try {
    size(int(args[0]), int(args[1]));
    //size(300,150);
    //size(640, 360);
  } catch (Exception e) {
    fullScreen();
  }
}

void setup() {
  int fps = 30;
  frameRate(fps);
  
  contador = 0;
  contadorAnterior = -1;
  pg = createGraphics(width, height);
  
  //limpa a pasta temp
  limpaArquivosTemporarios();
  
  //lista de quadros
  preparaQuadros();
  
  tempoAgora = millis();
  tempoUltimoDraw = millis();
}

void draw() {
  double deltaTime = (-tempoUltimoDraw + (tempoUltimoDraw = millis()));
  if (quadros.get(contador).getMovie() != null) {
    VLCJVideo videoDeAgora = quadros.get(contador).getMovie();
    if (!videoDeAgora.isPlaying()) {
      videoDeAgora.play();
    }
    image(videoDeAgora, 0, 0, width, height);
    if (aguardaIncrementaContador(quadros.get(contador).getTime())) { //se o contador for incrementado ou mudado.
      videoDeAgora.stop();
    }
  } else {
    pg.beginDraw();
    pg.background(quadros.get(contador).getBackground());
    pg.imageMode(CENTER);
    if (quadros.get(contador).getImage() != null) { //Se não houver imagem será mostrado apenas o background.
      pg.image(quadros.get(contador).getImage(), width/2, height/2);
    }
  
    pg.fill(quadros.get(contador).getColorFont());
    pg.textAlign(CENTER, CENTER);
    pg.textSize(quadros.get(contador).getSizeFont());
    pg.text(quadros.get(contador).getMessageFrame(deltaTime, tempoAgora), width/2, height/2);
    pg.endDraw();
    image(pg, 0, 0, width, height);
    aguardaIncrementaContador(quadros.get(contador).getTime());
  }
 
  if (contadorAnterior != contador) {
    quadros.get(contadorAnterior).playVoiceDestroy();
    if (quadros.get(contador).getTextToVoice() != null) {
      quadros.get(contador).playVoice();
    }
    contadorAnterior = contador;
  }
  tempoUltimoDraw = millis();
}

void limpaArquivosTemporarios() {
  File fp = new File(sketchPath("") + "imagens/temp");
  if (fp.isDirectory()) {
    for (String fName : fp.list()) {
      File f = new File(sketchPath("") + "imagens/temp" + "/" + fName);
      if (f.exists()) {
        f.delete();
      }
    }
  }
}

void preparaQuadros() {
  Logo logo = new Logo(pg, width, height, 0, 100, 1.5, 1.5);
  TideInfo mareInfo = new TideInfo("arquivos/cabedelo.csv");
  
  quadros = new HashMap<Integer, Frame>();
  Table  table = loadTable("config/quadros.config.csv", "header");
  int index = 0;
  int corDeFundo;
  String localizacaoImagemOuVideo;
  PImage image;
  int tempoDeApresentacao;
  float tamanhoDaFonte;
  int corDaFonte;
  String mensagem;
  String textoParaVoz;
  String url;
  int widthDaWebPage;
  int heightDaWebPage;
  int fatorDeEscalaDoDispositivo;
  int tempoDeEsperaDaWebPage;
  contadorAnterior = table.getRowCount()-1;
  for (TableRow row : table.rows()) {
    corDeFundo = !row.getString("cor de fundo").isEmpty()? unhex(row.getString("cor de fundo").toLowerCase().replace("0x","")): 0x000000FF;
    localizacaoImagemOuVideo = !row.getString("localizacao da(o) imagem/video").isEmpty()? row.getString("localizacao da(o) imagem/video"): null;
    tempoDeApresentacao = !row.getString("tempo de apresentacao (s)").isEmpty()? row.getInt("tempo de apresentacao (s)"): 6;
    tamanhoDaFonte = !row.getString("tamanho da fonte (%)").isEmpty()? row.getFloat("tamanho da fonte (%)"): 6.25;
    corDaFonte = !row.getString("cor da fonte").isEmpty()? unhex(row.getString("cor da fonte").toLowerCase().replace("0x","")): 0x00000000;
    mensagem = row.getString("mensagem/metodo");
    textoParaVoz = row.getString("texto para voz");
    widthDaWebPage = !row.getString("width da WebPage").isEmpty()? row.getInt("width da WebPage"): 640;
    heightDaWebPage = !row.getString("height da WebPage").isEmpty()? row.getInt("height da WebPage"): 360;
    fatorDeEscalaDoDispositivo = !row.getString("fator de escala do dispositivo WebPage").isEmpty()? row.getInt("fator de escala do dispositivo WebPage"): 4;
    url = row.getString("WebPage URL");
    tempoDeEsperaDaWebPage = !row.getString("tempo de espera da WebPage (s)").isEmpty()? row.getInt("tempo de espera da WebPage (s)"): tempoDeApresentacao;
    switch(row.getString("mensagem/metodo")) {
      case "$tideInfo.getTideInfo$":
         image = localizacaoImagemOuVideo != null? loadImage(localizacaoImagemOuVideo): null;
         quadros.put(index, new Frame(corDeFundo, image, null, tempoDeApresentacao*1000, int(tamanhoDaFonte*width/100), corDaFonte, "$tideInfo.getTideInfo$", textoParaVoz, null, mareInfo, null));
         index++;
         break;
      case "$logo.draw$":
        quadros.put(index, new Frame(corDeFundo, null, null, tempoDeApresentacao*1000, int(1), 0x00000000, "$logo.draw$", textoParaVoz, logo, null, null));
        index++;
        break;
      case "$webPageScreen.refreshImage$":
        quadros.put(index, new Frame(corDeFundo, null, null, tempoDeApresentacao*1000, int(tamanhoDaFonte*width/100), corDaFonte, "$webPageScreen.refreshImage$", textoParaVoz, null, null, new WebPageScreen(url, localizacaoImagemOuVideo, widthDaWebPage, heightDaWebPage, fatorDeEscalaDoDispositivo, tempoDeEsperaDaWebPage*1000)));
        index++;
        break;
      case "$video.play$":
        if (!localizacaoImagemOuVideo.contains(":")) {
          localizacaoImagemOuVideo = sketchPath("")+localizacaoImagemOuVideo;
        }
        
        VLCJVideo video = null;
        tempoDeApresentacao = 0;
        if (localizacaoImagemOuVideo != null && !localizacaoImagemOuVideo.isEmpty()) {
          video = new VLCJVideo(this);
          video.open(localizacaoImagemOuVideo);
          tempoDeApresentacao = (int) video.duration();
        }
        
        quadros.put(index, new Frame(corDeFundo, null, video, tempoDeApresentacao, int(tamanhoDaFonte*width/100), corDaFonte, "$video.play$",null, null, null, null));
        index++;
        break;
      default:
        image = localizacaoImagemOuVideo != null? loadImage(localizacaoImagemOuVideo): null;
        quadros.put(index, new Frame(corDeFundo, image, null, tempoDeApresentacao*1000, int(tamanhoDaFonte*width/100), corDaFonte, mensagem, textoParaVoz, null, null, null));
        index++;
    }
  }
}

boolean aguardaIncrementaContador(long aguardaTempo) {
  if (millis() - tempoAgora >= aguardaTempo) {
      contador = (contador + 1) % quadros.size();
      tempoAgora = millis();
      return true;
  }
  delay(1); //Aguarda um tempo para continuar o desenho na tela.
  return false;
}

void exit() {
  try {
    super.stop();
    super.exit();
    System.exit(0);
    System.gc();
  } catch (Exception ex){
    ex.printStackTrace();
  }
}
