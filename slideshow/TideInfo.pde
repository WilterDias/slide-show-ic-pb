import java.util.concurrent.atomic.AtomicInteger;

public class TideInfo {
  private Table table;
  private String path = "";
  
  private AtomicInteger lock;
  private Thread waitProcessThread;
  
  public TideInfo(String path) {
    this.path = path;
    table = loadTable(path, "header");
    this.lock = new AtomicInteger(0);
    checkTable();
  }
  
  private void executePython() {
    if (lock.get() == 0) {
      lock.incrementAndGet();
      boolean isWindowsSo = System.getProperty("os.name").toLowerCase().contains("windows");
      Process p = exec(isWindowsSo? "python": "python3", sketchPath("") + "scripts/tide_info_marinha.py", sketchPath("")); //Se não tiver instalado o python e os pacotes dará problema.
      //println("python "+sketchPath("") + "scripts/tide_info_marinha.py "+sketchPath(""));
      waitProcessThread = new WaitProcessThread(lock, p); //o lock e o processo é passado por referência
      waitProcessThread.start();
      try {
        waitProcessThread.join();
      } catch (Exception e) {println("Erro no método executePython da classe TideInfo.");}
    }
  }
  
  private void checkTable () {
    if (table == null) {
      executePython();
      table = loadTable(path, "header");
    }
  }
  
  public String getTideInfo() {
    checkTable ();
    
    String tide = "";
    try {
      for (TableRow row : table.rows()) {
        if (!row.getString("ano").isEmpty() && row.getInt("ano") != year()) {
          executePython();
          return "Tentando atualizar a Tábua de Maré\n para o ano atual.\nEsteje conectado a internet.";
        }
        
        if (row.getInt("ano") == year() 
            && row.getInt("mes") == month() 
            && row.getInt("dia") == day()) {
          tide += "Maré estimada: "+nf(day(),2,0)+"/"+nf(month(),2,0)+"/"+nf(year(),2,0)+"\n";
          tide += "Porto de Cabedelo, Marinha\n";
          tide += row.getString("mare 1")+"h"+"  "+nf(row.getFloat("nivel 1"),0,2)+"m\n";
          tide += row.getString("mare 2")+"h"+"  "+nf(row.getFloat("nivel 2"),0,2)+"m\n";
          tide += row.getString("mare 3")+"h"+"  "+nf(row.getFloat("nivel 3"),0,2)+"m\n";
          if(row.getString("mare 4").length() > 0) {
            tide += row.getString("mare 4")+"h"+"  "+nf(row.getFloat("nivel 4"),0,2)+"m";
          }
          break;
        }
      }
    } catch (Exception e) {
      executePython();
      println("Não foi possível ler o arquivo cabedelo.csv");
      return "Não foi possível ler\n o arquivo cabedelo.csv";
    }
    
    return tide;
  }
  
  public String getTideInfoTextToVoice() {
    String tide = "";
    try {
      for (TableRow row : table.rows()) {
        if (!row.getString("ano").isEmpty() && row.getInt("ano") != year()) {
          executePython();
          return null;
        }
        
        if (row.getInt("ano") == year() 
            && row.getInt("mes") == month() 
            && row.getInt("dia") == day()) {
          tide += "A Maré estimada para "+nf(day(),2,0)+" de "+nf(month(),2,0)+" de "+nf(year(),2,0)+"";
          tide += "no Porto de Cabedelo. De acordo com dados da Marinha é ";
          tide += "de "+nf(row.getFloat("nivel 1"),0,2)+" metros para as "+row.getString("mare 1")+" ,";
          tide += "de "+nf(row.getFloat("nivel 2"),0,2)+" metros para as "+row.getString("mare 2")+", ";
          tide += "de "+nf(row.getFloat("nivel 3"),0,2)+" metros para as "+row.getString("mare 3")+", ";
          if(row.getString("mare 4").length() > 0) {
            tide += "de "+nf(row.getFloat("nivel 4"),0,2)+" metros para as "+row.getString("mare 4");
          }
          break;
        }
      }
    } catch (Exception e) {
      return null;
    }
    
    return tide;
  }
}
