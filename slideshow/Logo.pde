public class Logo { 
  private float centerW;
  private float centerH;
  private float scaleW;
  private float scaleH;
  private float boatRotateDirection;
  private float boatRotate;
  private float theta;
  private float amplitude;
  private float frequencyWave;
  private float quantityPoints;
  private float widthWave;
  private float offsetWave;
  private float cloudMove;
  private float cloudDirection;
  
  private PGraphics pg;
  
  public Logo(PGraphics pg,
              float widthh,
              float heightt,
              float offsetCenterW,
              float offsetCenterH,
              float scaleW,
              float scaleH) {
    this.scaleW = scaleW*widthh/640;
    this.scaleH = scaleH*heightt/360;
    this.centerW =  (widthh + offsetCenterW*this.scaleW)/2;
    this.centerH = -(heightt + offsetCenterH*this.scaleH)/2;
    this.boatRotateDirection = 1;
    this.boatRotate = 0;
    this.theta = 0;
    this.amplitude = 8;
    this.frequencyWave = 1;
    this.quantityPoints = 100;
    this.widthWave = 320;
    this.offsetWave = -30;
    this.cloudMove = 0;
    this.cloudDirection = 1;
    
    this.pg = pg;
    frameRateLastNanos = 0;
    //size(640, 360);
  }
   
  public void draw(double deltaTime) {
    pg.scale(scaleW, scaleH); //Muda a escala de tudo
  
    pg.pushMatrix();
    pg.scale(1, -1);
    pg.translate(centerW/scaleW, centerH/scaleH);
    pg.fill( 255, 246, 64 );
    pg.stroke( 255, 246, 64 );
    pg.strokeWeight( 1 );
    sun();
    pg.popMatrix();  
    
    pg.pushMatrix();
    cloudMove = cloudMove + cloudDirection*((float)deltaTime/10);
    if (abs(cloudMove) > 14) {
      cloudMove = cloudDirection*14;
      cloudDirection = -1*cloudDirection;
    }
    pg.scale(1, -1);
    pg.translate(centerW/scaleW, centerH/scaleH);
    pg.rotate(cloudMove/-280.0);
    pg.fill(0xFFFFFFFF);
    pg.noStroke();
    clouds();
    pg.popMatrix();  
    
    pg.pushMatrix();
    boatRotate = boatRotate + boatRotateDirection*((float)deltaTime/10);
    if (abs(boatRotate) > 14) {
      boatRotate = boatRotateDirection*14;
      boatRotateDirection = -1*boatRotateDirection;
    }
    
    pg.scale(1, -1);
    pg.translate(centerW/scaleW, centerH/scaleH);
    pg.rotate(boatRotate/-280.0);
    pg.fill(0xFF3e4095);
    pg.noStroke();
    boat();
    clubName();
    pg.popMatrix();
    
    pg.pushMatrix();
    pg.scale(1, -1);
    pg.translate(centerW/scaleW, centerH/scaleH);
    pg.fill(0xFF3e4095);
    pg.noStroke();
    wave(1, (float)deltaTime/10);
    pg.fill(0xFF3e40FF);
    pg.noStroke();
    wave(2, (float)deltaTime/10);
    pg.popMatrix();  
  }
  
  public void sun() {
    float widthRay = 56;
    float variance = 16; //o quando o comprimento do raio varia
    int quantityPoints = 130;
    float angle = TWO_PI/quantityPoints;
    float radius;
    float centerX = 70 - random(0, 4);
    float centerY = 70 - random(0, 4);
    pg.beginShape();
    for(int i = 0; i < quantityPoints; i++) {
      radius = widthRay - random(0, variance);
      pg.line(centerX, centerY, radius*cos(angle*i)+centerX, radius*sin(angle*i)+centerY);
    }
    pg.endShape(CLOSE);
  }
  
  public void clouds() {
    float leftCloudX = -70 + cloudMove;
    float leftCloudY = 80;
    float rightCloudX = 80 + cloudMove;
    float rightCloudY = 20;
     
    pg.beginShape();
    // left cloud
    pg.ellipse(leftCloudX     , leftCloudY + 4,  70, 50);
    pg.ellipse(leftCloudX + 31, leftCloudY    ,  70, 40);
    pg.ellipse(leftCloudX - 31, leftCloudY    ,  70, 40);
      
    // right cloud
    pg.ellipse(rightCloudX     , rightCloudY     ,  70, 50);
    pg.ellipse(rightCloudX + 31, rightCloudY + 10,  70, 40);
    pg.ellipse(rightCloudX - 31, rightCloudY - 10,  70, 40);
    pg.endShape(CLOSE);
  }
  
  public void boat() {
    pg.beginShape();
    pg.triangle(0, 0, -85, 22.5, 0, 105);
    pg.endShape(CLOSE);
    for (int i=0; i<3; i++) {
      pg.beginShape();
      pg.vertex(((0-8*i)+210)/(-2), 0-8*i);
      pg.vertex(((-4-8*i)+210)/(-2), -4-8*i);
      pg.vertex(((-4-8*i)+230)/2, -4-8*i);
      pg.vertex(((0-8*i)+230)/2, 0-8*i);
      pg.endShape(CLOSE);
    }
  }
  
  public void clubName() {
    pg.beginShape();
    pg.scale(1, -1);
    pg.textAlign(LEFT, CENTER);
    //pg.textFont(createFont("Arial Bold", 18));
    pg.textSize(18);
    pg.text("IATE\nCLUBE\nDA\nPARAÍBA", 2, -58);
    pg.endShape(CLOSE);
    
  }
  
  public void wave(float attenuate, float delta) {
    pg.beginShape();
    theta += delta/20;//random(delta/40, delta/20);
    pg.vertex(-quantityPoints*widthWave/(2*100), 1.5*offsetWave);
    for (float k = -quantityPoints/2; k <= quantityPoints/2; k= k + 1) {
      pg.vertex((k/100)*widthWave,offsetWave + (amplitude/attenuate)*sin(TWO_PI*frequencyWave*(k/100)+(theta*attenuate)));
    }
    pg.vertex(quantityPoints*widthWave/(2*100), 1.5*offsetWave);
    pg.endShape(CLOSE);
  }
}
