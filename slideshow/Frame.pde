public class Frame {
  private Integer background;
  private PImage image;
  private VLCJVideo movie;
  private Integer time;
  private Integer sizeFont;
  private Integer colorFont; //RGBA
  private String message;
  private String textToVoice;
  private Logo logo;
  private TideInfo tideInfo;
  private WebPageScreen webPageScreen;
  
  private Process process;
  
  public Frame(
         Integer background,
         PImage image, 
         VLCJVideo movie,
         Integer time, 
         Integer sizeFont,
         Integer colorFont,
         String message,
         String textToVoice,
         Logo logo,
         TideInfo tideInfo,
         WebPageScreen webPageScreen) {
           this.background = background;
           this.time = time;
           this.sizeFont = sizeFont;
           this.colorFont = colorFont;
           this.message = message;
           this.textToVoice = textToVoice;
           this.logo = logo;
           this.tideInfo = tideInfo;
           this.webPageScreen = webPageScreen;
           if (this.webPageScreen != null) {
             //this.webPageScreen.setWaitTime(this.time);
             this.webPageScreen.setWaitProcess(false);
             this.image = loadImage(this.webPageScreen.getName());
           } else {
             this.image = image;
           }
           if (this.image != null) {
             this.image.resize(width, height);
           }
           if (movie != null) {
             this.movie = movie;
           }
  }

  public Integer getBackground() {
    return background;
  }
  
  public PImage getImage() {
    return image;
  }
  
  public VLCJVideo getMovie() {
    return movie;
  }
  
  public Integer getTime() {
    return time;
  }
  
  public Integer getSizeFont() {
    return sizeFont;
  }
  
  public Integer getColorFont() {
    return colorFont;
  }
  
  public String getMessage() {
    return message;
  }
  
  String getMessageFrame(double deltaTime, long timeStamp) {
    switch(this.message) {
      case "$tideInfo.getTideInfo$":
        return this.tideInfo.getTideInfo();
      case "$logo.draw$":
        this.logo.draw(deltaTime);
        return "";
      case "$webPageScreen.refreshImage$":
        this.webPageScreen.refreshImage();
        if (millis() - timeStamp >= time) { //Talvez mostrar uma mensagem se o frame estiver desatualizado. http://oliviertech.com/pt/java/how-to-get-a-file-creation-date/
          //this.image.set(0, 0, loadImage(this.webPageScreen.getName()));
          try {
            this.image = loadImage(this.webPageScreen.getName());
            this.image.resize(width, height);
          } catch (Exception e) {
            println("Erro no método getMessageFrame da classe Frame para o $webPageScreen.refreshImage$.");
          }
        }
        if (this.image == null) {
          println("Problema ao obter a imagem da WebPage no Frame.");
          return "Problema ao obter\n a WebPage."; 
        }
        return "";
      case "$video.play$":
      return "";
    }
    return this.message;
  }
  
  String getTextToVoice() {
    if (this.message.compareTo("$tideInfo.getTideInfo$") == 0){
      if (this.textToVoice.compareTo("$tideInfo.getTideInfo.enableVoice$")==0) {
        String tdi = this.tideInfo.getTideInfo();
        return !(tdi==null || tdi.isEmpty())? this.tideInfo.getTideInfoTextToVoice(): null;
      }
      return null;  
    } 
    return this.textToVoice;
  }
  
  void playVoice() {
    playVoiceDestroy();
    if (!(getTextToVoice()==null || getTextToVoice().isEmpty())) {
      boolean isWindowsSo = System.getProperty("os.name").toLowerCase().contains("windows");
      process =  exec(isWindowsSo? "python": "python3", sketchPath("") + "scripts/text_to_voice.py", getTextToVoice()); //Se não tiver instalado o python e os pacotes dará problema
    }
  }
  
  void playVoiceDestroy() {
    if (process != null ) {
      process.destroy();
    }
  }
  
  public void setBackground(Integer background) {
    this.background = background;
  }
  
  public void setImage(PImage image) {
    if (image != null) {
      image.resize(width, height);
    }
    this.image = image;
  }
  
  public void setTime(Integer time) {
    this.time = time;
  }
  
  public void setSizeFont(Integer sizeFont) {
    this.sizeFont = sizeFont;
  }
  
  public void setColorFont(Integer colorFont) {
    this.colorFont = colorFont;
  }
  
  public void setMessage(String message) {
    if (this.message.compareTo(message) != 0) {
      this.message = message;
    }
  }
  
  public void setTextToVoice(String textToVoice) {
    if (this.textToVoice.compareTo(textToVoice) != 0) {
      this.textToVoice = textToVoice;
    }
  } 
}
